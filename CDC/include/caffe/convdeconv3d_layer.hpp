/*
 *
 *  When: 2016/09/04
 *  Who: Zheng Shou
 *  Where: Columbia University
 *
 */

#ifndef CONVDECONV3D_LAYER_HPP_
#define CONVDECONV3D_LAYER_HPP_


#include <string>
#include <utility>
#include <vector>

#include "caffe/blob.hpp"
#include "caffe/common.hpp"
#include "caffe/layer.hpp"
#include "caffe/neuron_layers.hpp"
#include "caffe/loss_layers.hpp"
#include "caffe/data_layers.hpp"
#include "caffe/proto/caffe.pb.h"

namespace caffe {



template <typename Dtype>
class Convdeconv3DLayer : public Layer<Dtype> {
 public:
  explicit Convdeconv3DLayer(const LayerParameter& param)
      : Layer<Dtype>(param) {}
  virtual void SetUp(const vector<Blob<Dtype>*>& bottom,
      vector<Blob<Dtype>*>* top);

 protected:
  virtual Dtype Forward_cpu(const vector<Blob<Dtype>*>& bottom,
      vector<Blob<Dtype>*>* top);
  virtual Dtype Forward_gpu(const vector<Blob<Dtype>*>& bottom,
      vector<Blob<Dtype>*>* top);
  virtual void Backward_cpu(const vector<Blob<Dtype>*>& top,
      const bool propagate_down, vector<Blob<Dtype>*>* bottom);
  virtual void Backward_gpu(const vector<Blob<Dtype>*>& top,
      const bool propagate_down, vector<Blob<Dtype>*>* bottom);

  int kernel_size_;
  int kernel_depth_;
  int stride_;
  int temporal_stride_;
  int num_;
  int channels_;
  int pad_;
  int temporal_pad_;
  int length_;
  int height_;
  int width_;
  int num_output_;
  Blob<Dtype> col_buffer_;
  Blob<Dtype> swap_buffer_;
  shared_ptr<SyncedMemory> bias_multiplier_;
  bool bias_term_;
  int M_;
  int K_;
  int N_;
  int M0_;
  int N0_;
  int length_out_;
  int height_out_;
  int width_out_;

};

}


#endif /* CONVDECONV3D_LAYER_HPP_ */
